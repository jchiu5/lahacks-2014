package com.example.lahacksapp2014;


public class Task {
	
	
	int id; // task will be assigned unique id for removal purposes
	int timeSpent; // can put in an int for approximate time to spend.
	
	
	// temp
	
	int timeStartOnDevice; // temp, need to actually make a date class
	
	String name; // task will have a name
	String desc; // task will also have a description
	
	boolean selected;
	
	boolean completed; // is the task done?
	
	boolean started; // task has been started! set to true once it's started.
	boolean paused; // task is paused. conditions is that the task has been started

	// To be implemented later:
	
	// Date d; // need a date class that stores the time that the task was started/compelted.
	

	Task() // empty constructor

	{

	}
	
	
	Task (int i, int t, String n, String d)
	{
		id = i;
		timeSpent = t;
		name = n;
		desc = d;
		
		selected = false; // aha.
		completed = false;
		started = false;
		paused = false;
	}
	
	
	
	// This function pauses the task and the associated timer.
	boolean pauseTask()
	{
	if (!paused)
	{
		// pause the timer. 
		paused = true;
		return paused;
	}
	else 
		return false;
	}
	
	boolean resumeTask()
	{
		if (paused)
		{
			// resume the timer
			paused = false;
			return true;
		}
		else
			return false;
		
		
	}
	
	
	
	
	// bunch of setter functions 
	
	
	void setID(int i)
	{
		id = i;
	}
	
	
	void setName(String s)
	{
		name = s;
	}
	
	void setDesc(String s)
	{
		desc = s;	
	}
	
	void markComplete()
	{
		completed = true;
	}
	
	// return functions to be used
	
	void printAll()
	{
		System.out.println("Name of this task: " + name );
		System.out.println("Task ID#" + id);
		System.out.println("Task is completed?" + completed);
		System.out.println("Description:" + desc);
	}
	
	int getID()
	{
		return id;
	}
	
	String getName()
	{
		return name;
	}
	
	String getDesc()
	{
		return desc;
	}
	
	Boolean isComplete()
	{
		return completed;
	}
	
	
	
	
	
	void display()
	{
	// not sure if I need this, this is a temp place holder. 	
	}
	
	
}
