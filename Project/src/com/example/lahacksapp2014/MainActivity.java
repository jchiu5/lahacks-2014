package com.example.lahacksapp2014;

import java.util.ArrayList;

import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.graphics.Color;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;

import android.view.View.OnClickListener;


public class MainActivity extends Activity implements OnClickListener {
	
	
	Paint  paint = new Paint();
	

	Task sample1;
	Task sample2;
	Task sample3;
	Task sample4;
	Task sample5;
	
	
	ArrayList<Task> list = new ArrayList<Task>();
	
	
	CheckBox cb1;
	CheckBox cb2;
	CheckBox cb3;
	CheckBox cb4;
	CheckBox cb5;
	
	
	ArrayList<CheckBox> buttonList = new ArrayList<CheckBox>();
	
	EditText field; 
	
	
	@Override	
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		/// HERE
		setContentView(R.layout.activity_main);
	
		// Add all buttons to the arraylist of buttons, corresponding to their position...
		
		
		
		 cb1 = (CheckBox) findViewById(R.id.checkBox1);
		 cb2 = (CheckBox) findViewById(R.id.checkBox2);
		 cb3 = (CheckBox) findViewById(R.id.checkBox3);
		 cb4 = (CheckBox) findViewById(R.id.checkBox4);
		 cb5 = (CheckBox) findViewById(R.id.checkBox5);
		
		  
		
		System.out.println("buttonsMade");
		
		buttonList.add(cb1);
		buttonList.add(cb2);
		buttonList.add(cb3);
		buttonList.add(cb4);
		buttonList.add(cb5);
		
		// add more buttons later i suppose.
		
		
		// this field is the one we will use.
		field = ((EditText)findViewById(R.id.editText1)); 
		field.setVisibility(View.INVISIBLE);
		
		
		refreshCheckers(); // run this at init.

	}
	
	public void timerMethod()
	{
		// something to do with timers.
		
		
	}
	


	public void refreshCheckers()
	{
		for(int x = 0; x < buttonList.size(); x++)
		{
		buttonList.get(x).setVisibility(View.INVISIBLE); // make all checkboxes invisible.
		buttonList.get(x).setChecked(false); // make them all unchecked.
		}	
		
		for (int x = 0; x < list.size(); x++)
		{
			buttonList.get(x).setText(list.get(x).getName());
			buttonList.get(x).setVisibility(View.VISIBLE);
		}
		
	}
	
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	// Debug funct
	public void selectTask(View view)
	{
		if (cb1.isShown()) 
		{
			System.out.println("select1");	
		}
		
		if (cb2.isShown())
		{
			System.out.println("select2");
		}
		
		if (cb3.isShown())
		{
			System.out.println("select3");
		}

		if (cb4.isShown())
		{
			System.out.println("select4");
		}
		
		if (cb5.isShown())
		{
			System.out.println("select5");
		}

	}
	
	public void delTask(View view)
	{
		
		
		for (int x = 0; x < buttonList.size(); x++)
		{
			if (buttonList.get(x).isChecked())
			{
				list.get(x).selected = true;
			}
			
		}	
		
		for (int x = 0; x < list.size(); x++)
		{
				
			if (list.get(x).selected)
			{
			System.out.println("Removing task: " + list.get(x).name);
				list.remove(x);
				x--;
			}
		}
				
		refreshCheckers();	
		
	}	
	public void makeTask(View view)
	{
		if (!field.isShown()) // first time pressed
		{
			field.setVisibility(View.VISIBLE); // make the field visible.
		}
		
		else
		{
			Task t = new Task(1, 25, field.getText().toString(), "blank");
			list.add(t);
			field.setVisibility(View.INVISIBLE); // make the field visible.
		}
		/*
		
		 sample1 = new Task(1, 1, "laHack", "is kool");
		 sample2= new Task(1, 1, "what", "is kool");
		 sample3 = new Task(1, 1, "hellno", "is kool");
		
		
		if (list.size() < 3)
		list.add(sample1); // for now to test functionality
		else if (list.size()  == 3)
			list.add(sample2); // for now to test functionality
		else if (list.size() > 3)
			list.add(sample3);
		// add it to the list, and have to update the appropriate button.
	*/
		
		
		
		
		refreshCheckers();
	
		

	}
	
	


	@Override
	public void onClick(View arg0) {
		// TODO Auto-generated method stub
		
	}
	
}
