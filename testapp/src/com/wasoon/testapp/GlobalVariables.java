package com.wasoon.testapp;

import java.util.Timer;
import java.util.TimerTask;

import android.app.Application;
import android.content.Context;
import android.media.MediaPlayer;
import android.os.Vibrator;

public class GlobalVariables extends Application {
	
	public GlobalVariables()
	{}
	
	//MaxTime
	private static int maxTime = 1500;
	public static int getMaxTime()
	{return maxTime;}
	public static void setMaxTime(int number)
	{maxTime = number;}
	
	//Time
	private static int time;
	public static int getTime()
	{return time;}
	public static void setTime(int number)
	{time = number;}

	//Timer
	static Timer timer = new Timer();
	private static boolean timerActive;
	public static boolean getTimerActive()
	{return timerActive;}
	public static void setTimerActive(boolean bool)
	{timerActive = bool;}
	
	//Pause
	private static boolean isPaused = true;
	public static boolean getIsPaused()
	{return isPaused;}
	public static void setIsPaused(boolean bool)
	{isPaused = bool;} 
	
	//Global timer
	public class AppTimer extends TimerTask
	{
		public void run()
		{
        	if (!isPaused)
        	{time--;}
			if (time <= 0)
			{
				timer.cancel();
				timer.purge();
				timerActive = false;
				playSound();
				vibrateLong();
			}
		}
	}
	
	private static MediaPlayer mySound;
	private static boolean notified;
	private static void playSound()
	{
		if (!notified)
		{
			mySound = MediaPlayer.create(MyApplication.getAppContext(), R.raw.ding);
			mySound.start();
		}
	}
	
	private static void vibrateLong()
    {
    	Vibrator v = (Vibrator) MyApplication.getAppContext().getSystemService(Context.VIBRATOR_SERVICE);
    	v.vibrate(1000);
    }
	
	//Reset
	public static void resetTimer()
	{
		timer.cancel();
		setTime(maxTime);
		setTimerActive(true);
		GlobalVariables g = new GlobalVariables();		
		timer = new Timer();
		timer.schedule(g.new AppTimer(),0,1000);
	}
	
	
	//Tasks
	private static String[] tasks = {"","","","",""};
	public static String[] getTasks()
	{return tasks;}
	public static void setTasks(int index, String data)
	{tasks[index] = data;}
	public static void setTasks(String[] stuff)
	{tasks = stuff;}
	

}
