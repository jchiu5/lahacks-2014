package com.wasoon.testapp;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Point;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Vibrator;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBarActivity;
import android.view.Display;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class TimeCounter extends ActionBarActivity {
	private static TextView timeCount;
	private static Handler handler;
	private Button paures;
	private Button reset;
	private static ImageView avatarThing;
	private static int[] idle = {R.drawable.idle1, R.drawable.idle2};
	private static int[] clap = {R.drawable.clap1, R.drawable.clap2};
	private static int[] tired = {R.drawable.tired1, R.drawable.tired2};
	private static int cycle = 0;
	
	@TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2) @Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_time_counter);

		if (savedInstanceState == null) {
			getSupportFragmentManager().beginTransaction()
					.add(R.id.container, new PlaceholderFragment()).commit();
		}
		
		Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int width = size.x;
        int height = size.y;
        
        avatarThing = (ImageView) findViewById(R.id.imageView1);
        avatarThing.setX(width/8);
        avatarThing.setY(height/8);
        
        timeCount = (TextView) findViewById(R.id.timer);
        timeCount.setX(width/3);
        timeCount.setY(height / 2 + 30);
        
        paures = (Button) findViewById(R.id.paures);
        paures.setWidth(width/2);
        paures.setX(width/4);
        paures.setY(height * 2 /3);
        paures.setText("Resume");
        reset = (Button) findViewById(R.id.reset);
        reset.setWidth(width/2);
        reset.setX(width/4);
        reset.setY(height * 2 / 3 + 80);
        
        if (!GlobalVariables.getTimerActive())
        {
        	paures.setText("Start");
        }
        
        handler = new Handler();
        runnable.run();
	}

	private Runnable runnable = new Runnable() 
	{

	    public void run() 
	    {
    		display();
    		handler.postDelayed(this, 10);
	    }
	};

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.time_counter, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event)
	{
		if (keyCode == KeyEvent.KEYCODE_BACK)
		{
			handler.removeCallbacks(runnable);
		}
		return super.onKeyDown(keyCode, event);
	}

	/**
	 * A placeholder fragment containing a simple view.
	 */
	public static class PlaceholderFragment extends Fragment {

		public PlaceholderFragment() {
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			View rootView = inflater.inflate(R.layout.fragment_time_counter,
					container, false);
			return rootView;
		}
	}
	
	public static void display()
	{
		timeCount.setText(Integer.toString(GlobalVariables.getTime()/60) + "m " + Integer.toString(GlobalVariables.getTime()%60) + "s");
		
		if (cycle >= 99) {cycle = 0;} else {cycle++;}
		
		if (Avatar.getState() == 0)
		{avatarThing.setImageResource(idle[cycle/50]);}
		else if (Avatar.getState() == 1)
		{avatarThing.setImageResource(clap[cycle/50]);}
		else
		{avatarThing.setImageResource(tired[cycle/50]);}
		
		if (GlobalVariables.getTime() == 0)
		{
			Avatar.setState(1);
		}
	}

	
	public void pauseClicked(View view)
	{
		vibrate();
		if (paures.getText() == "Start")
		{
			Avatar.setState(0);
			paures.setText("Pause");
			GlobalVariables.setIsPaused(false);
			GlobalVariables.resetTimer();
		}
		else if (GlobalVariables.getIsPaused())
		{
			Avatar.setState(0);
			paures.setText("Pause");
			GlobalVariables.setIsPaused(false);
		}
		else
		{
			Avatar.setState(2);
			paures.setText("Resume");
			GlobalVariables.setIsPaused(true);
		}
	}
	
	public void resetClicked(View view)
	{
		vibrate();
		paures.setText("Start");
		GlobalVariables.setIsPaused(true);
		GlobalVariables.resetTimer();
	}
	
	private void vibrate()
    {
    	Vibrator v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
    	v.vibrate(50);
    }
}
