package com.wasoon.testapp;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.graphics.Point;
import android.os.Build;
import android.os.Bundle;
import android.os.Vibrator;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBarActivity;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;


public class MainActivity extends ActionBarActivity {
	
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2) @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.container, new PlaceholderFragment())
                    .commit();
        }
        
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int width = size.x;
        int height = size.y;
        
        //Today
        final Button button1 = (Button) findViewById(R.id.button1);
        button1.setX(width / 4);
        button1.setWidth(width / 2);
        button1.setY(height / 2);
        button1.setText("Today");
        
        //Options
        final Button button2 = (Button) findViewById(R.id.button2);
        button2.setX(width / 4);
        button2.setWidth(width / 2);
        button2.setY(height / 2 + 80);
        button2.setText("Options");
        
        //Quit
        final Button button3 = (Button) findViewById(R.id.button3);
        button3.setX(width/4);
        button3.setWidth(width/2);
        button3.setY(height / 2 + 160);
        button3.setText("Quit");
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {

        public PlaceholderFragment() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_main, container, false);
            return rootView;
        }
    }

    public void button1Press(View view)
    {
    	vibrate();
    	Intent intent = new Intent(this, MyTasks.class);
    	startActivity(intent);
    }
    
    public void button2Press(View view)
    {
    	vibrate();
    	Intent intent = new Intent(this, OptionPage.class);
    	startActivity(intent);
    }
    
    public void button3Press(View view)
    {
    	vibrate();
    	finish();
    }
    
    private void vibrate()
    {
    	Vibrator v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
    	v.vibrate(50);
    }
}
