package com.wasoon.testapp;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Point;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Vibrator;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBarActivity;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

@TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2) public class OptionPage extends ActionBarActivity {
	Handler handler;
	TextView dtd;
	TextView options;
	TextView time;
	Button timePlus;
	Button timeMinus;
	Button done;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_option_page);

		if (savedInstanceState == null) {
			getSupportFragmentManager().beginTransaction()
					.add(R.id.container, new PlaceholderFragment()).commit();
		}
		
		Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int width = size.x;
        int height = size.y;
        
        options = (TextView) findViewById(R.id.textView1);
        options.setX(width/4);
        
		dtd = (TextView) findViewById(R.id.defaultTimerDuration);
		dtd.setX(20);
		dtd.setY(height/6 + 10);
		
		timeMinus = (Button) findViewById(R.id.timeMinus);
		timeMinus.setX(width/2 - 30);
		timeMinus.setY(height/6 + 10);
		
		time = (TextView) findViewById(R.id.time);
		time.setX(width/2 + 20);
		time.setY(height/6 + 11);
		
		timePlus = (Button) findViewById(R.id.timePlus);
		timePlus.setX(width/2 + 110);
		timePlus.setY(height/6 + 10);
		
		done = (Button) findViewById(R.id.done);
		done.setX(width/2 - 120);
		done.setY(height * 2 / 3 + 50);
		
		
        handler = new Handler();
        runnable.run();
	}

	private Runnable runnable = new Runnable() 
	{

	    public void run() 
	    {
    		display();
    		handler.postDelayed(this, 10);
	    }
	};
	
	private void display()
	{
		time.setText(Integer.toString(GlobalVariables.getMaxTime()/60) + "m " + Integer.toString(GlobalVariables.getMaxTime()%60) + "s");
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.option_page, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	/**
	 * A placeholder fragment containing a simple view.
	 */
	public static class PlaceholderFragment extends Fragment {

		public PlaceholderFragment() {
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			View rootView = inflater.inflate(R.layout.fragment_option_page,
					container, false);
			return rootView;
		}
	}
	
	public void cTimeMinus(View view)
	{
		vibrate();
		GlobalVariables.setMaxTime(GlobalVariables.getMaxTime() - 300);
	}
	
	public void cTimePlus(View view)
	{
		vibrate();
		GlobalVariables.setMaxTime(GlobalVariables.getMaxTime() + 301);
	}
	
	public void doneClick(View view)
	{
		vibrate();
		finish();
	}
	
	private void vibrate()
    {
    	Vibrator v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
    	v.vibrate(50);
    }
}
